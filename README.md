# Commader Gulp Styles Dynamic

<p>It is a small project created by the commander to compile sass,scss and stylus tasks in css.</p>
 
![MIT License](https://img.shields.io/badge/lincense-MIT-yellow?style=for-the-badge) 
![npm: version (tag)](https://img.shields.io/badge/npm-v6.4.3-blue?style=for-the-badge)
![gulp: version (tag)](https://img.shields.io/badge/gulp-v3.9.1-orange?style=for-the-badge)
![node License](https://img.shields.io/badge/node-v8.16.0-green?style=for-the-badge) 


[![GitHub stars](https://img.shields.io/github/stars/jugaz12/gulp-styles?style=social)](https://github.com/jugaz12/gulp-styles)
[![GitHub forks](https://img.shields.io/github/forks/jugaz12/gulp-styles?style=social)](https://github.com/jugaz12/gulp-styles/network)

## Installation

```bash
$ npm install comander-gulp-styles
```


#### Command to Compile

```bash
$ compile-styles scss 'entry' --sc 'ouput' 
```

```bash
$ compile-styles stylus 'entry' --st 'ouput' 
```

#### Example

```bash
$ compile-styles scss 'frontend/static/styles/**/*.scss' --sc 'build/css'
```

```bash
$ compile-styles scss 'app/css/**/*.styl' --st 'build/css'
```
