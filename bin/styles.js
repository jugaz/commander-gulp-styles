#!/usr/bin/env node

'use strict';

var
    autoprefixer = require('autoprefixer'),
    gulp = require('gulp-param')(require('gulp'), process.argv),
    mkdirp = require('mkdirp'),
    path = require('path'),
    postcss = require('gulp-postcss'),
    program = require('commander'),
    rimraf = require('rimraf'),
    sass = require('gulp-sass'),
    stylus = require('gulp-stylus')

;


/* ######################## PLUGINS ######################## */
var Plugins = [
    autoprefixer({
        overrideBrowserslist: ['last 20 version']
    })
];


/* ######################## OPTIONS ######################## */
var options = {};


/* ######################## VERSION ######################## */
program

    .version(
        'gulp-styles-cli version: ' + require('../package.json').version + '\n' +
        'sass version: ' + require('gulp-sass/package.json').version + '\n' +
        'stylus version: ' + require('gulp-stylus/package.json').version
    )
    .option('-m, --mkdirp <path>', 'create folder', createFolder)
    .option('-r, --rimraf <path>', 'delete folder', deleteFolder)


/* ######################## CREATE FOLDERS ######################## */
function createFolder(dir) {
    mkdirp(dir, function (err) {
        if (err) {
            console.error(err)
        } else {
            console.log(dir)
        }
    })
}


/* ######################## DELETE FOLDERS ######################## */
function deleteFolder(dir) {
    rimraf(dir, function (err) {
        if (err) {
            console.error(err)
        } else {
            console.log(dir)
        }
    })
}

/* ######################## GULP SCSS ######################## */
// example node index.js scss 'test/**/*.scss'  --odir 'build/css'
program
    .command('scss <input>')
    .option("--sc [options]")
    .action((input, options) => {
        var input = options.input || options.parent.rawArgs[3]
        var ouput = options.ouput || options.parent.rawArgs[5]
        return gulp.src(input)
            .pipe(sass({
                outputStyle: 'compressed'
            }).on('error', sass.logError))
            .pipe(postcss(Plugins))
            .pipe(gulp.dest(ouput));
    })

/* ######################## GULP STYLUS ######################## */
// example node index2.js scss 'test/**/*.scss'  --odir 'build/css'
program
    .command('stylus <input>')
    .option("--st [options]")
    .action((input, options) => {
        var input = options.input || options.parent.rawArgs[3]
        var ouput = options.ouput || options.parent.rawArgs[5]
        console.log("input =>", input)
        console.log("ouput =>", ouput)
        return gulp.src(input)
            .pipe(stylus({
                compress: true
            }))
            .pipe(postcss(Plugins))
            .pipe(gulp.dest(ouput));
    })

program.parse(process.argv);
